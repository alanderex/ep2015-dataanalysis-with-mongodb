"""
presets artist & nemesis
create sample document simplified_document in ordered fashion
"""
from collections import OrderedDict

artist = "Taylor Swift"
nemesis = "Katy Perry"

# if nltk is not fully installed, fallback to these stopwords
_stopwords = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', 'your', 'yours', 'yourself',
              'yourselves', 'he', 'him', 'his', 'himself', 'she', 'her', 'hers', 'herself', 'it', 'its', 'itself',
              'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', 'these',
              'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do',
              'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while',
              'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during', 'before',
              'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again',
              'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each',
              'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than',
              'too', 'very', 's', 't', 'can', 'will', 'just', 'don', 'should', 'now']

# HOST = "m2.chartgeist.com:27017,m1.chartgeist.com:27017"
# REPLICASET = "rs0"
HOST = "m2.chartgeist.com:27057"
REPLICASET = None
DB = "cg_preproduction"
USER = "tweTJebceDvOmsutoTjAgOomWuSleOdgEywIcJhiAcrctRvRxU"
PASS = "zVnThhpw@9QuLr,fcNGWKCmwmuaKfF}qjjrTYcohKvGaPdBY7q"
AUTHDB = "admin"
playlists = "itunes_playlists"
#
# HOST = None
# DB = "cg_preproduction"
# USER = None
# PASS = None
# playlists = "playlists"
