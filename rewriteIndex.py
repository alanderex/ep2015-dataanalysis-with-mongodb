import pymongo
from config import *

db = pymongo.MongoClient(HOST, replicaSet=REPLICASET)[DB]
db.authenticate(USER, PASS, source=AUTHDB)
print(db[playlists].count())
cursor = db[playlists].find({"$where": "this.info.artistName.length > 124"}, {'info.artistName': 1})
for c in cursor:
    print(c)
    db[playlists].update({'_id': c.get('_id')}, {"$set": {'info.artistName': c.get('info.artistName', "")[:124],
                                                          'info.artistName_org': c.get('info.artistName', "")}})
    # print(c.get('info.artistName'))
    # print(c.get('info.artistName', "")[:124])
    # exit(0)
